# python_portfolio

My first, small programs and functions written in Python.

Programs:

Anagrams

The program looks for anagrams in a paragraph of text and lists the pairs of anagrams. Condition: the text must be in one paragraph (no new lines).

functions:

camel

The function takes a CamelCasedString as an argument and transforms it into a snake_cased_string. A different separator can be given as the second argument.

upsidedown

The function takes a string with ASCII characters as an argument and returns the upside down version of that string.